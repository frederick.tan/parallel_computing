#!/bin/bash

log_dir="log_`date +%F_%H-%M-%S`"
result_dir=result/${log_dir}
mkdir -p ${result_dir}
for size in {50..10000..50}; do
	./heat ${size} >> ${result_dir}/result.base
	for nth in {1..64..1}; do
		./heat_omp ${size} ${nth} >> ${result_dir}/result.parallel
	done
done

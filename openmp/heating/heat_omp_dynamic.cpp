/*
 * File: 
 *
 *
 **/
#include <iostream>
#include <string>
#include <vector>
#include <cmath>

#include <omp.h>
#include <sys/time.h>

// Load the multidimensional array template
#include "arrayff.hxx"

// Load the template functions to draw the hot and cold spots
#include "draw_openmp.hxx"

void fix_boundaries3(Array<float, 2>& h)
{
  const double wwid = 0.005;
  const float T0 = 273.0;
  const float T1 = T0 + 50;
  const float T2 = T0 + 8.0;
  const float T3 = T0 + 30;
  const float T4 = T0 + 10;
  const float T5 = T0 + 5 ;
  const float T6 = T0 - 100;

  const int npixy = h.length[0];
  const int npixx = h.length[1];
  int x = 0;
  int y = 0;
#pragma omp parallel shared(h) private(y)
  {
#pragma omp for schedule(dynamic)
    for (y = 0; y < npixy; ++y) {
      h(y, 0) = T6;
      h(y, npixx-1) = T6;
    }
  }
#pragma omp parallel shared(h) private(x)
  {
#pragma omp for schedule(dynamic)
    for (x = 0; x < npixx; ++x) {
      h(0, x) = T6;
      h(npixy-1, x) = T6;
    }
  }

  const double x0 = 0.2;
  const double y0 = 0.7;
  const double r = 0.05;
  put_circ(h, T1, x0, y0, r);
  connectx(h, T1, x0+r, T2, 0.7, y0, wwid);
  put_rect(h, T2, 0.7, 0.76, 0.65, 0.8);

  connecty(h, T3, 0.1, T2, 0.65, 0.73, wwid);
  put_rect(h, T3, 0.6, 0.76, 0.05, 0.1);

  connecty(h, T3, 0.1, T4, 0.3, 0.62, wwid);
  connectx(h, T5, 0.2, T4, 0.62,  0.3, wwid);
  put_rect(h, T5, 0.15, 0.2, 0.2, 0.4);

  // Cold finger heat sink
  put_rect(h, T6, 0.0, 0.55, 0.5, 0.53);
}

int main(int argc, char *argv[])
{
  const float tol = 0.00001;
  const int npix = atoi(argv[1]);
  const int nth = atoi(argv[2]);
  const int npixx = npix;
  const int npixy = npix;
  struct timeval tStart, tMid, tEnd;

  Array<float, 2> h(npixy, npixy), g(npixy, npixx);
  const int nrequired = npixx * npixy;
  const int ITMAX = 1000000;

  int iter = 0;
  int nconverged = 0;

  double start = omp_get_wtime();
  fix_boundaries2(h);
  dump_array<float, 2>(h, "plate0.fit");

  int x = 0;
  int y = 0;

  double mid = omp_get_wtime();
  omp_set_num_threads(nth);
  do {
#pragma omp parallel shared(g, h) private(x, y)
    {
#pragma omp for schedule(static)
      for (y = 1; y < npixy-1; ++y) {
        for (x = 1; x < npixx-1; ++x) {
          g(y, x) = 0.25 * (h(y, x-1) + h(y, x+1) + h(y-1, x) + h(y+1,x));
        }
      }
    }
    fix_boundaries3(g);

    float dhg = 0.0;
    int nconvergedLoc = 0;
    nconverged = 0;
#pragma omp parallel shared(h, g) private(x, y, dhg, nconvergedLoc)
    {
      nconvergedLoc = 0;
      dhg = 0.0;
#pragma omp for schedule(dynamic) reduction(+:nconverged)
      for (y = 0; y < npixy; ++y) {
        for (x = 0; x < npixx; ++x) {
          dhg = std::fabs(g(y, x) - h(y, x));
          h(y, x) = g(y, x);
          if (dhg < tol) {
		 ++nconverged;
	  }
        }
      }
    }
    ++iter;
  } while (nconverged < nrequired && iter < ITMAX);

  double end = omp_get_wtime();
  std::cout << nth << "," << omp_get_num_threads() << "," << npix << "," << (end - start) << "," << (end - mid) << "," << (mid - start) << "," << iter << std::endl;
}

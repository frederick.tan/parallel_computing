
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mpi.h"
#include "randomdata.h"

using namespace std;

// The comparison function to use with the library qsort
// function. This will tell qsort to sort the numbers in ascending
// order.
int compare(const void* x1, const void* x2) {
  const float* f1 = (float*)x1;
  const float* f2 = (float*)x2;
  float diff = *f1 - *f2;

  return (diff < 0) ? -1 : 1;
}

int check(float *data,int nitems) {
  double sum=0;
  int sorted=1;
  int i;

  for(i=0;i<nitems;i++) {
    sum+=data[i];
    if(i && data[i]<data[i-1]) sorted=0;
  }
}

// Sequential implementation of the bucket sort routine. The full
// range x1 to x2 will be divided into a number of equally spaced
// subranges according to the number of buckets. All the buckets are
// contained in the single one dimensional array "bucket".
void bucket_sort_scatter(float *data, int ndata, float x1, float x2, int nbuckets, float *bigBucket, int myId)
{
  int i, count;
  float stepSize = (x2 - x1) / nbuckets;
  // The number of items thrown into each bucket. We would expect each
  // bucket to have a similar number of items, but they won't be
  // exactly the same. So we keep track of their numbers here.
  int* nitems = (int*)malloc(nbuckets * sizeof(int));

  for (i = 0; i < nbuckets; ++i) nitems[i] = 0;

  if (myId == 0) {
    // Toss the data items into the correct bucket
    for (i = 0; i < ndata; ++i) {
      // What bucket does this data value belong to?
      int bktno = (int)floor((data[i] - x1) / stepSize);
      int idx = bktno * ndata + nitems[bktno];
      // Put the data value into this bucket
      bigBucket[idx] = data[i];
      ++nitems[bktno];
    }
  }

  int dataCount = ndata / nbuckets;
  float *recvBuffer = new float[dataCount * 2];
  int root = 0;
  int mySize = 0;
  MPI::COMM_WORLD.Scatter(bigBucket, dataCount * 2, MPI_FLOAT, recvBuffer, dataCount * 2, MPI_FLOAT, root); 
  MPI::COMM_WORLD.Scatter(nitems, 1, MPI_INT, &mySize, 1, MPI_INT, root); 
  if (mySize > 0) {
    check(recvBuffer, mySize);
    qsort(recvBuffer, mySize, sizeof(float), compare);
    check(recvBuffer, mySize);
  }
  int dest = 0;
  /* Determine the displacement in the final array */
  int* disp = (int*)malloc(nbuckets * sizeof(int));
  for (i = 0; i < nbuckets; ++i) disp[i] = 0;
    if(myId == 0)
    {
        disp[0] = 0;
        for(i = 1; i < nbuckets+1; ++i)
            disp[i] = disp[i-1] + nitems[i-1];
  }
  MPI::COMM_WORLD.Gatherv(recvBuffer, mySize, MPI_FLOAT, data, nitems, disp, MPI_FLOAT, dest);
  
  delete []recvBuffer;
}

// Sequential implementation of the bucket sort routine. The full
// range x1 to x2 will be divided into a number of equally spaced
// subranges according to the number of buckets. All the buckets are
// contained in the single one dimensional array "bucket".
void bucket_sort_all_to_all(float *data, int ndata, float x1, float x2, int nbuckets, float *bigBucket, int myId)
{
  int i, count;
  float stepSize = (x2 - x1) / nbuckets;
  // The number of items thrown into each bucket. We would expect each
  // bucket to have a similar number of items, but they won't be
  // exactly the same. So we keep track of their numbers here.
  //int* nitems = (int *)calloc(nbuckets, sizeof(int));
  int* nitems = (int*)malloc(nbuckets * sizeof(int));
  for (i = 0; i < nbuckets; ++i) nitems[i] = 0;

  if (myId == 0) {
    // Toss the data items into the correct bucket
    for (i = 0; i < ndata; ++i) {
      // What bucket does this data value belong to?
      int bktno = (int)floor((data[i] - x1) / stepSize);
      int idx = bktno * ndata + nitems[bktno];
      // Put the data value into this bucket
      bigBucket[idx] = data[i];
      ++nitems[bktno];
    }
  }

  int dataCount = ndata / nbuckets;
  float *recvBuffer = new float[dataCount * 2];
  int root = 0;
  int mySize = 0;
  MPI::COMM_WORLD.Scatter(bigBucket, dataCount * 2, MPI_FLOAT, recvBuffer, dataCount * 2, MPI_FLOAT, root); 
  MPI::COMM_WORLD.Scatter(nitems, 1, MPI_INT, &mySize, 1, MPI_INT, root); 
  if (mySize > 0) {
    check(recvBuffer, mySize);
    qsort(recvBuffer, mySize, sizeof(float), compare);
    check(recvBuffer, mySize);
  }
  // Sort each bucket using the standard library qsort routine. Note
  // that we need to input the correct number of items in each bucket
  int dest = 0;
  /* Determine the displacement in the final array */
  int* disp = (int*)malloc(nbuckets * sizeof(int));
  for (i = 0; i < nbuckets; ++i) disp[i] = 0;
    if(myId == 0)
    {
        disp[0] = 0;
        for(i = 1; i < nbuckets+1; ++i)
            disp[i] = disp[i-1] + nitems[i-1];
  }
  MPI::COMM_WORLD.Gatherv(recvBuffer, mySize, MPI_FLOAT, data, nitems, disp, MPI_FLOAT, dest);
  
  delete []recvBuffer;
}

int main(int argc, char **argv)
{
  MPI::Init(argc,argv);
  const int myId = MPI::COMM_WORLD.Get_rank();
  const int nBuckets = MPI::COMM_WORLD.Get_size();
  const float xMax = 1100.0;
  const float xMin = 10.0;
    double t1, t2, t3, t4;

  enum EMPIMode
  {
    mpimNone,
    mpimScatter,
    mpimAllToAll
  };

  EMPIMode mode = mpimScatter;
  float *data;
  float *buckets;
  int nItems = 10000;
  if (argc >= 2) {
    nItems = atoi(argv[1]);
  }
  if (argc >= 3) {
    mode = (EMPIMode)(atoi(argv[2]));
  }

  if (myId == 0) {
        t1 = MPI::Wtime();
    RandomData<float> randomData(nItems, xMax, xMin);

    float *rData = randomData.GetData();
    data = new float[randomData.mData.size()];
    memcpy(data, rData, randomData.mData.size() * sizeof(float));

    buckets = (float*)calloc((nBuckets * nItems), sizeof(float*));
        t2 = MPI::Wtime();
  }

  switch (mode) {
  case mpimNone: {
    break;
  }
  case mpimScatter: {
    bucket_sort_scatter(data, nItems, xMin, xMax, nBuckets, buckets, myId);
        t3 = MPI::Wtime();
    break;
  }
  case mpimAllToAll: {
    break;
  }
  }


  if (myId == 0) {
    double delta1 = t2 - t1;
    double delta2 = t3 - t1;
    std::cout << "td:" << delta1 << "," << delta2 << endl;
    check(data,nItems);
    delete []data;
  }

    MPI::Finalize();
  return 0;
}

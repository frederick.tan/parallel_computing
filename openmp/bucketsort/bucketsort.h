#ifndef _BUCKET_SORT_H_
#define _BUCKET_SORT_H_

#include <memory>
#include "mpi.h"

class BucketSort
{
public:
	BucketSort();
	int Init();
private:
	void Sort();
private:
	int bucketCount;
	int minValue;
	int maxValue;
	int dataCount;
	float *data;
	float *bucket;
};
#endif

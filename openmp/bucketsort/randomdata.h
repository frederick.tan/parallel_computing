/*
 * Filename: Randomdata.h
 * Description: Generates random data
 * Date Created: 14/10/2018
 **/
#ifndef _RANDOM_DATA_H_
#define _RANDOM_DATA_H_

#include <math.h>
#include <memory>
#include <vector>

template <class T>
class RandomData
{
public:
	RandomData(const int nItems, T const& maxValue, T const& minValue)
		: mDataCount(nItems)
		, mMaxValue(maxValue)
		, mMinValue(minValue)
	{
		Generate();
	}
	T* GetData()
	{
		return mData.data();
	}
private:
	void Generate()
	{
		// TODO: Find a random number generator that supports template
		// drand48 doesn't support template
		for (int i=0; i<mDataCount; i++) {
			T value = GetRandomNumber();
			mData.push_back(value);
		}
	}
	float GetRandomNumber()
	{
		float value = drand48() * (mMaxValue - mMinValue - 1) + mMinValue;
		return value;
	}
public:
	std::vector<T> mData;
private:
	int mDataCount;
	T mMaxValue;
	T mMinValue;
};

#endif

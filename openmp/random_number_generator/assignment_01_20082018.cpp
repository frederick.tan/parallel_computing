#include <stdio.h>
#include <stdlib.h>

#include <cmath>
#include <iostream>

#include "mpi.h"
#include "jump_table.h"

using namespace std;
typedef unsigned long ULONG;

static const ULONG m = 4294967296;
static const ULONG diameter = 65536;
static const ULONG radius = diameter / 2;
static const ULONG radiusSquared = radius * radius;

bool IsInside(ULONG n)
{
    ULONG nx = n % diameter;
    ULONG ny = n / diameter;
    ULONG distanceSquared = ((radius - nx) * (radius - nx)) + ((radius - ny) * (radius - ny));
    return (distanceSquared < radiusSquared);
}

//always use argc and argv, as mpirun will pass the appropriate parms.
int main(int argc,char* argv[])
{
    MPI::Init(argc,argv);
    // What is my ID and how many processes are in this pool?
    const int myid = MPI::COMM_WORLD.Get_rank();
    const int numproc = MPI::COMM_WORLD.Get_size();
    const ULONG C = JumpTable::C[numproc - 1];
    const ULONG A = JumpTable::A[numproc - 1];
    const ULONG pointPerProcess =  m / numproc;

    ULONG nprev = 0;
    double t1, t2, t3;

    if (myid == 0) {
        const ULONG a = 1664525;
        const ULONG c = 1013904223;
        // calculate f here
        // generate for numproc
        nprev = (a * 0 + c) % m;
        t1 = MPI::Wtime();
        for (int id = 1; id < numproc; id++)
        {
            ULONG n = (a * nprev + c) % m;
            nprev = n;
            MPI::COMM_WORLD.Send(&n, 1, MPI::UNSIGNED_LONG, id, 0);
        }
        t2 = MPI::Wtime();
    } else {
        MPI::COMM_WORLD.Recv(&nprev, 1, MPI::UNSIGNED_LONG, 0, 0);
    }
    ULONG NInside = 0;
    for (ULONG i = 1; i <= pointPerProcess; i++) {
        if (IsInside(nprev)) {
            NInside++;
        }
        nprev = (A * nprev + C) % m;
    }
    if (myid == 0) {
        double t3 = MPI::Wtime();
        for (int id = 1; id < numproc; id++)
        {
            ULONG NInside_sub = 0;//, NTotal_n;
            MPI::COMM_WORLD.Recv(&NInside_sub, 1, MPI::UNSIGNED_LONG, id, 1);
            NInside += NInside_sub;
        }
        double t4 = MPI::Wtime();
        //std::cout << "Points inside: " << NInside << std::endl;
        double pi = (double)(NInside * 4.0)/m;
        double serial_t1 = t2 - t1;
        double serial_t2 = t3 - t1;
        double total_t = t4 - t1;
        cout.precision(100);
        std::cout << "The approx value is: " << pi << std::endl;
        std::cout << serial_t1 << "," << serial_t2 << "," << total_t << endl;
    } else {
        MPI::COMM_WORLD.Send(&NInside, 1, MPI::UNSIGNED_LONG, 0, 1);
    }

    MPI::Finalize();
}

#!/bin/bash

PBS_FILE_PREFIX=./assignment01/pbs_20082018_new/a01
LOG_FILE_PREFIX=./assignment01/log_20082018_new/a01

mkdir -p ./assignment01/pbs_20082018_new
mkdir -p ./assignment01/log_20082018_new

for node_count in {1..5}
do
    max_ppn=$((${node_count}*8))
    for i in $(seq 1 $max_ppn)
    do
	echo $i
        PBS_FILENAME=${PBS_FILE_PREFIX}_${node_count}_${max_ppn}_$i.pbs
        LOG_FILENAME=${LOG_FILE_PREFIX}_${node_count}_${max_ppn}_$i.log
        echo "#PBS -j oe -o ${LOG_FILENAME} -l nodes=${node_count}:ppn=8 -q pp">${PBS_FILENAME}
        echo "echo Read from \$PBS_NODEFILE">>${PBS_FILENAME}
        echo "# Make the appropriate changes to this line">>${PBS_FILENAME}
        echo "mpiexec -machinefile \$PBS_NODEFILE -np $i /home/s16227617/mpi_examples/assignment_01 1000000">>${PBS_FILENAME}
    done
done

for node_count in {1..5}
do
    max_ppn=$((${node_count}*8))
    for i in $(seq 1 $max_ppn)
    do
        PBS_FILENAME=${PBS_FILE_PREFIX}_${node_count}_${max_ppn}_$i.pbs
        qsub ${PBS_FILENAME}
        while true
        do
            STATUS=`qstat`
            if [ -z "${STATUS}" ]
            then
                break
            else
                sleep 1
            fi
        done
    done
done

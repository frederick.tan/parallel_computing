#!/bin/bash

PBS_FILE_PREFIX=./assignment01/pbs_v3/a01
LOG_FILE_PREFIX=./assignment01/log_v3/a01

mkdir -p ./assignment01/pbs_v3
mkdir -p ./assignment01/log_v3

node_count=5

for i in {1..40}
do
	PBS_FILENAME=${PBS_FILE_PREFIX}_v3_${node_count}_$i.pbs
	LOG_FILENAME=${LOG_FILE_PREFIX}_v3_${node_count}_$i.log
	echo "#PBS -j oe -o ${LOG_FILENAME} -l nodes=5:ppn=8 -q pp">${PBS_FILENAME}
	echo "echo Read from \$PBS_NODEFILE">>${PBS_FILENAME}
	echo "# Make the appropriate changes to this line">>${PBS_FILENAME}
	echo "mpiexec -machinefile \$PBS_NODEFILE -np $i /home/s16227617/mpi_examples/assignment_01_v03 1000000">>${PBS_FILENAME}
done

for i in {1..40}
do
	PBS_FILENAME=${PBS_FILE_PREFIX}_v3_${node_count}_$i.pbs
	qsub ${PBS_FILENAME}
	while true
	do
		STATUS=`qstat`
		if [ -z "${STATUS}" ]
		then
			break
		else
			sleep 1
		fi
	done
done

#!/bin/bash

PBS_FILE_PREFIX=./assignment01/pbs/a01
LOG_FILE_PREFIX=./assignment01/log/a01

mkdir -p ./assignment01/pbs
mkdir -p ./assignment01/log

for i in {1..200}
do
	for node_count in {3..5}
	do
		PBS_FILENAME=${PBS_FILE_PREFIX}_8_${node_count}_$i.pbs
		LOG_FILENAME=${LOG_FILE_PREFIX}_8_${node_count}_$i.log
		echo "#PBS -j oe -o ${LOG_FILENAME} -l nodes=${node_count}:ppn=8 -q pp">${PBS_FILENAME}
		echo "echo Read from \$PBS_NODEFILE">>${PBS_FILENAME}
		echo "# Make the appropriate changes to this line">>${PBS_FILENAME}
		echo "mpiexec -machinefile \$PBS_NODEFILE -np $i /home/s16227617/mpi_examples/assignment_01 1000000">>${PBS_FILENAME}
	done
done

for i in {1..200}
do
	for node_count in {3..5}
	do
		PBS_FILENAME=${PBS_FILE_PREFIX}_8_${node_count}_$i.pbs
		qsub ${PBS_FILENAME}
		while true
		do
			STATUS=`qstat`
			if [ -z "${STATUS}" ]
			then
				break
			else
				sleep 1
			fi
		done
	done
done

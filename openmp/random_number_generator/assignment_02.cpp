//first.cpp Adding numbers using two nodes C++ version
#include <stdio.h>
#include <stdlib.h>

#include <cmath>
#include <iostream>
#include <vector>

#include "mpi.h"
#include "jump_table.h"

#define IS_INSIDE(x) 1
using namespace std;
typedef unsigned long ULONG;

static const ULONG m = pow(2, 32);
static const ULONG diameter = pow(2, 16);
static const ULONG radius = diameter / 2;
static const ULONG radiusSquared = pow(radius, 2);

bool IsInside(ULONG n)
{
	ULONG nx = n % diameter;
	ULONG ny = n / diameter;
	ULONG distanceSquared = pow(radius - nx, 2) + pow(radius - ny, 2);
	return (distanceSquared <= radiusSquared);
}

//always use argc and argv, as mpirun will pass the appropriate parms.
int main(int argc,char* argv[])
{
	MPI::Init(argc,argv);
    // What is my ID and how many processes are in this pool?
	const int myid = MPI::COMM_WORLD.Get_rank();
	const int numproc = MPI::COMM_WORLD.Get_size();
	const ULONG C = JumpTable::C[numproc - 1];
	const ULONG A = JumpTable::A[numproc - 1];
    const ULONG pointPerProcess =  m / numproc;

    //std::cout << "This is id " << myid << " out of " << numproc << std::endl;
    //std::cout << "C: " << C << std::endl;
    //std::cout << "Points per process: " << pointPerProcess << std::endl;

    if (myid == 0) {
    	const ULONG a = 1664525;
    	const ULONG c = 1013904223;
        // calculate f here
        // generate for numproc
        std::vector<int> N;
        ULONG n0 = (a * 0 + c) % m;
        ULONG nprev = n0;
        //N.push_back(n0);
        double t1 = MPI::Wtime();
        for (int id = 1; id < numproc; id++)
        {
            //ULONG n = (a * N[id-1] + c) % m;
            ULONG n = (a * nprev + c) % m;
			nprev = n;
            //N.push_back(n);
            MPI::COMM_WORLD.Send(&n, 1, MPI::UNSIGNED_LONG, id, 0);
        }
        double t2 = MPI::Wtime();
		ULONG NInside = 0;
		nprev = n0;
		for (ULONG i = 0; i < pointPerProcess; i++) {
			ULONG nnext = (A * nprev + C) % m;
			nprev = nnext;
			//cout << "nnext: " << nnext << endl;
			if (IsInside(nnext)) {
				// Radius = sqrt(m) / 2
				// Acircle = Pi * R ^ 2
				NInside++;
			}
		}
        double t3 = MPI::Wtime();
        for (int id = 1; id < numproc; id++)
        {
			ULONG NInside_sub;//, NTotal_n;
            MPI::COMM_WORLD.Recv(&NInside_sub, 1, MPI::UNSIGNED_LONG, id, 1);
            //MPI_Recv(NTotal, 1, MPI::UNSIGNED_LONG, 0, 2);
			//cout << "Received NInside: " << NInside_sub << " from id " << id << endl;
			NInside += NInside_sub;
        }
        double t4 = MPI::Wtime();
		//std::cout << "Points inside: " << NInside << std::endl;
		double pi = (double)(NInside * 4)/m;
		double serial_t1 = t2 - t1;
		double serial_t2 = t3 - t1;
		double total_t = t4 - t1;
		cout.precision(17);
		//std::cout << "The approx value is: " << pi << std::endl;
		std::cout << serial_t1 << "," << serial_t2 << "," << total_t << endl;
    } else {
        ULONG n0;
        MPI::COMM_WORLD.Recv(&n0, 1, MPI::UNSIGNED_LONG, 0, 0);
		ULONG nprev = n0;
		ULONG NInside = 0;
		//cout << "N0: " << n0 << endl;
		for (ULONG i = 0; i < pointPerProcess; i++) {
			ULONG nnext = (A * nprev + C) % m;
			nprev = nnext;
			//cout << "nnext: " << nnext << endl;
			if (IsInside(nnext)) {
				// Radius = sqrt(m) / 2
				// Acircle = Pi * R ^ 2
				NInside++;
			}
		}
		//cout << "NInside: " << NInside << " for id " << myid << endl;
        MPI::COMM_WORLD.Send(&NInside, 1, MPI::UNSIGNED_LONG, 0, 1);
    }

    MPI::Finalize();
}

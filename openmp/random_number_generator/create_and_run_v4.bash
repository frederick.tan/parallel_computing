#!/bin/bash

PBS_FILE_PREFIX=./assignment01/pbs_v4_b/a01
LOG_FILE_PREFIX=./assignment01/log_v4_b/a01

mkdir -p ./assignment01/pbs_v4_b
mkdir -p ./assignment01/log_v4_b

for i in {1..40}
do
	PBS_FILENAME=${PBS_FILE_PREFIX}_v4_$i.pbs
	LOG_FILENAME=${LOG_FILE_PREFIX}_v4_$i.log
	echo "#PBS -j oe -o ${LOG_FILENAME} -l nodes=5:ppn=8 -q pp">${PBS_FILENAME}
	echo "echo Read from \$PBS_NODEFILE">>${PBS_FILENAME}
	echo "# Make the appropriate changes to this line">>${PBS_FILENAME}
	echo "mpiexec -machinefile \$PBS_NODEFILE -np $i /home/s16227617/mpi_examples/assignment_01 1000000">>${PBS_FILENAME}
done

for i in {1..40}
do
	PBS_FILENAME=${PBS_FILE_PREFIX}_v4_$i.pbs
	qsub ${PBS_FILENAME}
	while true
	do
		STATUS=`qstat`
		if [ -z "${STATUS}" ]
		then
			break
		else
			sleep 1
		fi
	done
done

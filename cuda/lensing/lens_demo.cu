/**
 * Filename: lens_demo.cu
 * Description: CUDA implementation of the Gravitional Lensing problem
 * Author: Frederick Tan
 **/
#include <ctime>

#include <iostream>
#include <string>

#include <cmath>

#include <cuda.h>

#include "lenses.h"
#include "arrayff.hxx"


using namespace std;
// Global variables! Not nice style, but we'll get away with it here.

// Boundaries in physical units on the lens plane
const float WL  = 2.0;
const float XL1 = -WL;
const float XL2 =  WL;
const float YL1 = -WL;
const float YL2 =  WL;

// Source star parameters. You can adjust these if you like - it is
// interesting to look at the different lens images that result
const float rsrc = 0.1;      // radius
const float ldc  = 0.5;      // limb darkening coefficient
const float xsrc = 0.0;      // x and y centre on the map
const float ysrc = 0.0;
const float rsrc2 = rsrc * rsrc;

// Pixel size in physical units of the lens image. You can try finer
// lens scale which will result in larger images (and take more
// time).
//const float lens_scale = 0.005;
//const float lens_scale = 0.003;
//const float lens_scale = 0.00025;

// Used to time code. OK for single threaded programs but not for
// multithreaded programs. See other demos for hints at timing CUDA
// code.
double diffclock(clock_t clock1,clock_t clock2)
{
  double diffticks = clock1 - clock2;
  double diffms = (diffticks * 1000) / CLOCKS_PER_SEC;
  return diffms; // Time difference in milliseconds
}

// Implement lens equation, given the lens position (xl, yl) and the
// lens system configuration, shoot a ray back to the source position
// (xs, ys)
__global__ void shoot_cuda(float* xlens, float* ylens, float* eps, float *pixels, int nlenses, int npixx, const long pixelCount, const float lens_scale)
{
  // We have to convert the thread number to indeces (iy, ix)
  int threadId = blockDim.x * blockIdx.x + threadIdx.x;
  if (threadId >= pixelCount) return;
  int iy = threadId / npixx;
  int ix = threadId % npixx;
  float yl = YL1 + iy * lens_scale;
  float xl = XL1 + ix * lens_scale;
  float xs = xl;
  float ys = yl;
  for (int p = 0; p < nlenses; ++p) {
    float dx = xl - xlens[p];
    float dy = yl - ylens[p];
    float dr = dx * dx + dy * dy;
    xs -= eps[p] * dx / dr;
    ys -= eps[p] * dy / dr;
  }
  float xd = xs - xsrc;
  float yd = ys - ysrc;
  float sep2 = xd * xd + yd * yd;
  if (sep2 < rsrc2) {
    float mu = sqrt(1 - sep2 / rsrc2);
    pixels[threadId] = 1.0 - ldc * (1 - mu);
  }
}

int main(int argc, char* argv[]) 
{
  // Set up lensing system configuration - call example_1, _2, _3 or
  // _n as you wish. The positions and mass fractions of the lenses
  // are stored in these arrays
  if (argc < 4) {
    std::cout << "Usage: " << argv[0] << " <scale (float)> <n lenses (int)> <threads per block>" << std::endl;
    exit(1);
  }
  const float lens_scale = atof(argv[1]);
  const int nlenses = atoi(argv[2]);
  int threadsPerBlock = atoi(argv[3]);
  float* xlens;
  float* ylens;
  float* eps;
  set_example_n(nlenses, &xlens, &ylens, &eps);
  size_t deltaSize = nlenses * sizeof(float);
  float *x_deltas = (float*)malloc(deltaSize);
  float *y_deltas = (float*)malloc(deltaSize);

  // Copy the sample to device memory
  float *d_xlens, *d_ylens, *d_eps;
  // Size of the lens image
  const int npixx = static_cast<int>(floor((XL2 - XL1) / lens_scale)) + 1;
  const int npixy = static_cast<int>(floor((YL2 - YL1) / lens_scale)) + 1;

  // Put the lens image in this array
  Array<float, 2> lensim(npixy, npixx);

  clock_t tstart = clock();

  cudaMalloc(&d_xlens, deltaSize);
  cudaMalloc(&d_ylens, deltaSize);
  cudaMalloc(&d_eps, deltaSize);

  cudaMemcpy(d_xlens, xlens, deltaSize, cudaMemcpyHostToDevice);
  cudaMemcpy(d_ylens, ylens, deltaSize, cudaMemcpyHostToDevice);
  cudaMemcpy(d_eps, eps, deltaSize, cudaMemcpyHostToDevice);


  // Draw the lensing image map here. For each pixel, shoot a ray back
  // to the source plane, then test whether or or not it hits the
  // source star
  long pixelCount = (npixy * npixx);
  size_t totalSize = sizeof(float) * pixelCount;
  float *d_pixels;
  clock_t tcp1 = clock();
  cudaMalloc(&d_pixels, totalSize);
  clock_t tcp2 = clock();
  cudaMemcpy(d_pixels, lensim.buffer, totalSize, cudaMemcpyHostToDevice);
  clock_t tcp3 = clock();
  int blocksPerGrid = (pixelCount +  threadsPerBlock - 1) / threadsPerBlock;

  shoot_cuda<<<blocksPerGrid, threadsPerBlock>>>(d_xlens, d_ylens, d_eps, d_pixels, nlenses, npixx, pixelCount, lens_scale);
  cudaDeviceSynchronize();
  clock_t tcp4 = clock();
  cudaMemcpy(lensim.buffer, d_pixels, totalSize, cudaMemcpyDeviceToHost);

  clock_t tend = clock();
  double tms = diffclock(tend, tstart);
  std::cout << blocksPerGrid << "," << threadsPerBlock << "," << pixelCount << "," << lens_scale << "," << nlenses << "," << tms;
  std::cout << "," << diffclock(tcp3, tstart) << "," << diffclock(tcp3, tcp2) << "," << diffclock(tend, tcp4) << std::endl;

  delete[] xlens;
  delete[] ylens;
  delete[] eps;
  delete[] x_deltas;
  delete[] y_deltas;
  cudaFree(d_xlens);
  cudaFree(d_ylens);
  cudaFree(d_eps);
  cudaFree(d_pixels);
}


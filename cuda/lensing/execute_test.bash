#!/bin/bash

log_dir="log_`date +%F_%H-%M-%S`"
result_dir=result/${log_dir}
mkdir -p ${result_dir}

for size in $(seq 0.0 0.0005 0.3); do
	scale=`echo 0.05 - ${size} | bc -l`
	for ncount in $(seq 10 100 1010); do
		for nth in $(seq 64 64 1024); do
			./lens_demo_cu ${scale} ${ncount} ${nth}>>${result_dir}/result.parallel
		done
		./lens_demo ${scale} ${ncount}>>${result_dir}/result.base
	done
done

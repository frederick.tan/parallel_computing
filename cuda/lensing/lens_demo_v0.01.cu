/* 
   159735 Parallel Programming

   Startup program for sequential implementation of simulation by ray
   tracing of gravitational lensing.
 */
#include <ctime>

#include <iostream>
#include <string>

#include <cmath>

#include <cuda.h>

#include "lenses.h"
#include "arrayff.hxx"

// Global variables! Not nice style, but we'll get away with it here.

// Boundaries in physical units on the lens plane
const float WL  = 2.0;
const float XL1 = -WL;
const float XL2 =  WL;
const float YL1 = -WL;
const float YL2 =  WL;

// Used to time code. OK for single threaded programs but not for
// multithreaded programs. See other demos for hints at timing CUDA
// code.
double diffclock(clock_t clock1,clock_t clock2)
{
  double diffticks = clock1 - clock2;
  double diffms = (diffticks * 1000) / CLOCKS_PER_SEC;
  return diffms; // Time difference in milliseconds
}

// Implement lens equation, given the lens position (xl, yl) and the
// lens system configuration, shoot a ray back to the source position
// (xs, ys)
__global__ void shoot_cuda(float* xl, float* yl, float* xlens, float* ylens, float* eps, float* x_deltas, float* y_deltas, int nlenses)
{
  float dx, dy, dr;
  //xs = xl;
  //ys = yl;
  //for (int p = 0; p < nlenses; ++p) {
  //  dx = xl - xlens[p];
  //  dy = yl - ylens[p];
  //  dr = dx * dx + dy * dy;
  //  xs -= eps[p] * dx / dr;
  //  ys -= eps[p] * dy / dr;
  //}
  int p = blockDim.x * blockIdx.x + threadIdx.x;
  if (p < nlenses) {
    dx = *xl - xlens[p];
    dy = *yl - ylens[p];
    dr = dx * dx + dy * dy;
    x_deltas[p] = eps[p] * dx / dr;
    y_deltas[p] = eps[p] * dy / dr;
  }
}

int main(int argc, char* argv[]) 
{
  // Set up lensing system configuration - call example_1, _2, _3 or
  // _n as you wish. The positions and mass fractions of the lenses
  // are stored in these arrays
  float* xlens;
  float* ylens;
  float* eps;
  const int nlenses = set_example_2(&xlens, &ylens, &eps);
  size_t delta_size = nlenses * sizeof(float);
  float *x_deltas = (float*)malloc(delta_size);
  float *y_deltas = (float*)malloc(delta_size);
  std::cout << "# Simulating " << nlenses << " lens system" << std::endl;

  // Copy the sample to device memory
  float *d_xlens, *d_ylens, *d_eps;
  float *d_x_deltas, *d_y_deltas;
  cudaMalloc(&d_xlens, 2);
  cudaMalloc(&d_ylens, 2);
  cudaMalloc(&d_eps, 2);
  cudaMalloc(&d_x_deltas, delta_size);
  cudaMalloc(&d_y_deltas, delta_size);

  cudaMemcpy(d_xlens, xlens, 2, cudaMemcpyHostToDevice);
  cudaMemcpy(d_ylens, ylens, 2, cudaMemcpyHostToDevice);
  cudaMemcpy(d_eps, eps, 2, cudaMemcpyHostToDevice);

  // Source star parameters. You can adjust these if you like - it is
  // interesting to look at the different lens images that result
  const float rsrc = 0.1;      // radius
  const float ldc  = 0.5;      // limb darkening coefficient
  const float xsrc = 0.0;      // x and y centre on the map
  const float ysrc = 0.0;

  // Pixel size in physical units of the lens image. You can try finer
  // lens scale which will result in larger images (and take more
  // time).
  const float lens_scale = 0.005;
  //const float lens_scale = 0.00025;

  // Size of the lens image
  const int npixx = static_cast<int>(floor((XL2 - XL1) / lens_scale)) + 1;
  const int npixy = static_cast<int>(floor((YL2 - YL1) / lens_scale)) + 1;
  std::cout << "# Building " << npixx << "X" << npixy << " lens image" << std::endl;

  // Put the lens image in this array
  Array<float, 2> lensim(npixy, npixx);

  clock_t tstart = clock();

  // Draw the lensing image map here. For each pixel, shoot a ray back
  // to the source plane, then test whether or or not it hits the
  // source star
  const float rsrc2 = rsrc * rsrc;
  float xl, yl, xs, ys, sep2, mu;
  float *d_xl, *d_yl;
  float xd, yd;
  int numuse = 0;
  cudaMalloc(&d_xl, 1);
  cudaMalloc(&d_yl, 1);
  
  int threadsPerBlock = 256;
  int blocksPerGrid = (nlenses + threadsPerBlock - 1) / threadsPerBlock;
  std::cout << "Launching a grid of "
         << blocksPerGrid << " "
         << threadsPerBlock * blocksPerGrid
         << " threads" << std::endl;
  for (int iy = 0; iy < npixy; ++iy) 
    for (int ix = 0; ix < npixx; ++ix) { 

    // YOU NEED TO COMPLETE THIS SECTION OF CODE
    // need position on lens in physical units
      yl = YL1 + iy * lens_scale;
      xl = XL1 + ix * lens_scale;
      cudaMemcpy(d_xl, &xl, 1, cudaMemcpyHostToDevice);
      cudaMemcpy(d_yl, &yl, 1, cudaMemcpyHostToDevice);

    // shoot a ray back to the source plane - make the appropriate
    // call to shoot() in lenses.h
    // Invoke kernel
      //std::cout << "Launching a grid of "
      //      << blocksPerGrid << " "
      //      << threadsPerBlock * blocksPerGrid
      //      << " threads" << std::endl;
      shoot_cuda<<<blocksPerGrid, threadsPerBlock>>>(d_xl, d_yl, d_xlens, d_ylens, d_eps, d_x_deltas, d_y_deltas, nlenses);
      cudaMemcpy(x_deltas, d_x_deltas, delta_size, cudaMemcpyDeviceToHost);
      cudaMemcpy(y_deltas, d_y_deltas, delta_size, cudaMemcpyDeviceToHost);
      xs = xl;
      ys = yl;
      for (int i = 0; i < nlenses; i++) {
        xs -= x_deltas[i];
        ys -= y_deltas[i];
      }

    // does the ray hit the source star?
    xd = xs - xsrc;
    yd = ys - ysrc;
    sep2 = xd * xd + yd * yd;
    if (sep2 < rsrc2) {
      mu = sqrt(1 - sep2 / rsrc2);
      lensim(iy, ix) = 1.0 - ldc * (1 - mu);
    }
  }

  clock_t tend = clock();
  double tms = diffclock(tend, tstart);
  std::cout << "# Time elapsed: " << tms << " ms " << numuse << std::endl;

  // Write the lens image to a FITS formatted file. You can view this
  // image file using ds9
  dump_array<float, 2>(lensim, "lens02cu.fit");

  delete[] xlens;
  delete[] ylens;
  delete[] eps;
  delete[] x_deltas;
  delete[] y_deltas;
  cudaFree(d_xlens);
  cudaFree(d_ylens);
  cudaFree(d_eps);
  cudaFree(d_x_deltas);
  cudaFree(d_y_deltas);
}


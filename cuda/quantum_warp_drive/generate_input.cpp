/* 159.735 Semester 2, 2016.  Ian Bond, 3/10/2016
 
 Sequential version of the N-sphere counting problem for Assignment
 5. Two alternative algorithms are presented.

 Note: a rethink will be needed when implementing a GPU version of
 this. You can't just cut and paste code.

 To compile: g++ -O3 -o nsphere nsphere.cpp
 (you will get slightly better performance with the O3 optimization flag)
*/
#include <cstdlib>
#include <cmath>

#include <iostream>
#include <string>

#include <vector>

const long MAXDIM = 10;
const double RMIN = 2.0;
const double RMAX = 8.0;

using namespace std;

int main(int argc, char* argv[]) 
{
  // You can make this larger if you want
  long ntrials = 100;
  long maxdim = MAXDIM;
  long mindim = 1;

  if (argc >= 2) {
    ntrials = atol(argv[1]);
  }

  if (argc >= 3) {
    maxdim = atol(argv[2]);
  }

  if (argc >= 4) {
    mindim = atol(argv[3]);
  }

  for (long n = 0; n < ntrials; ++n) {

    // Get a random value for the hypersphere radius between the two limits
    const double r = drand48() * (RMAX - RMIN) + RMIN;

    // Get a random value for the number of dimensions between 1 and
    // MAXDIM inclusive
    const long  nd = lrand48() % (maxdim - mindim) + mindim;
    std::cout << r << " " << nd << std::endl;
  }
}


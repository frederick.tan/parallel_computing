/* 159.735 Semester 2, 2016.  Ian Bond, 3/10/2016
 
 Sequential version of the N-sphere counting problem for Assignment
 5. Two alternative algorithms are presented.

 Note: a rethink will be needed when implementing a GPU version of
 this. You can't just cut and paste code.

 To compile: g++ -O3 -o nsphere nsphere.cpp
 (you will get slightly better performance with the O3 optimization flag)
*/
#include <cstdlib>
#include <cmath>

#include <iostream>
#include <string>

#include <vector>

#include <sys/time.h>

const long MAXDIM = 10;
const double RMIN = 2.0;
const double RMAX = 8.0;

using namespace std;
long powlong(long n, long k)
/* Evaluate n**k where both are long integers */
{
  long p = 1;
  for (long i = 0; i < k; ++i) p *= n;
  return p;
}

/*----------------------------------------------------------------------------*/

void convert(long num, long base, std::vector<long>& index)
/* Convert a decimal number into another base system - the individual
   digits in the new base are stored in the index array. */
{
  //cout << "convert in" << endl;
  const long ndim = index.size();
  for (long i = 0; i < ndim; ++i) index[i] = 0;
  long idx = 0;
  while (num != 0) {
    long rem = num % base;
    num = num / base;
    index[idx] = rem;
    ++idx;
  }
  //cout << "convert out" << endl;
}

long count_in_v1(long ndim, double radius)
/* 
   Version 1 of the counting algorithm. Given:

   ndim   -> number of dimensions of the hypersphere
   radius -> radius of the hypersphere

   count the number of integer points that lie wholly within the
   hypersphere, assuming it is centred on the origin.
*/
{
  const long halfb = static_cast<long>(floor(radius));
  const long base = 2 * halfb + 1;
  const double rsquare = radius * radius;

  // This is the total number of points we will need to test.
  const long ntotal = powlong(base, ndim);

  long count = 0;

  // Indices in x,y,z,.... 
  std::vector<long> index(ndim, 0);

  // Loop over the total number of points. For each visit of the loop,
  // we covert n to its equivalent in a number system of given "base".
  for (long n = 0; n < ntotal; ++n) {
    convert(n, base, index);
    double rtestsq = 0;
    for (long k = 0; k < ndim; ++k) {
      double xk = index[k] - halfb;
      rtestsq += xk * xk;
    }
    if (rtestsq < rsquare) ++count;
  }

  return count;
}

/*----------------------------------------------------------------------------*/

void addone(std::vector<long>& index, long base, long i)
/* Add one to a digital counter of given base. When one digit hits
   maximum, it is necessary to carry one over into the next
   column. This is done recursively here. */
{
  //cout << "addone in" << endl;
  long ndim = index.size();
  long newv = index[i] + 1;
  if (newv >= base) {
    index[i] = 0;
    if (i < ndim - 1) addone(index, base, i+1);
  }
  else {
    index[i] = newv;
  }
  //cout << "addone out" << endl;
}

long count_in_v2(long ndim, double radius)
/* 
   Version 2 of the counting algorithm. Given:

   ndim   -> number of dimensions of the hypersphere
   radius -> radius of the hypersphere

   count the number of integer points that lie wholly within the
   hypersphere, assuming it is centred on the origin.
*/
{
  //cout << "count_in_v2 in" << endl;
  const long halfb = static_cast<long>(floor(radius));
  const long base = 2 * halfb + 1;
  const double rsquare = radius * radius;
  const long ntotal = powlong(base, ndim);

  long count = 0;

  // This is the counter
  std::vector<long> index(ndim, 0);

  // Loop over the total number of points to test, ticking over the
  // counter as we go.
  for (long n = 0; n < ntotal; ++n) {
    double rtestsq = 0;
    for (long k = 0; k < ndim; ++k) {
      double xk = index[k] - halfb;
      rtestsq += xk * xk;
    }
    if (rtestsq < rsquare) ++count;
    addone(index, base, 0);
  }
  //cout << "count_in_v2 out" << endl;
  return count;
}

int main(int argc, char* argv[]) 
{
  // You can make this larger if you want
	struct timeval tStart, tEnd;
	struct timeval tStart2, tEnd2;

	if (argc < 3) {
		cout << "Usage: " << argv[0] << " <radius> <n dimension>" << endl;
		exit(1);
	}

	const double radius = atof(argv[1]); 
  const long ndim = atol(argv[2]);

  if ((radius < RMIN) || (radius > RMAX)) {
    cout << "Radius should be between " << RMIN << " and " << RMAX << "." << endl;
    exit(1);
  }

  if ((ndim < 1) || (ndim > MAXDIM)) {
    cout << "Dimension should be less than " << MAXDIM << "." << endl;
    exit(1);
  }

  const long halfb = static_cast<long>(floor(radius));
  const long base = 2 * halfb + 1;
  const long ntotal = powlong(base, ndim);

	gettimeofday(&tStart, NULL);
  const long num1 = count_in_v1(ndim, radius);
	gettimeofday(&tEnd, NULL);
	gettimeofday(&tStart2, NULL);
  const long num2 = count_in_v2(ndim, radius);
	gettimeofday(&tEnd2, NULL);
  //std::cout << " -> " << num1 << " " << num2 << std::endl;
	long int msEnd = tEnd.tv_sec * 1000 * 1000 + tEnd.tv_usec;
	long int msStart = tStart.tv_sec * 1000 * 1000 + tStart.tv_usec;
	long int msEnd2 = tEnd2.tv_sec * 1000 * 1000 + tEnd2.tv_usec;
	long int msStart2 = tStart2.tv_sec * 1000 * 1000 + tStart2.tv_usec;
	std::cout << radius << "," << ndim << "," << ntotal << "," << (msEnd - msStart) << "," << (msEnd2 - msStart2) << std::endl;
}


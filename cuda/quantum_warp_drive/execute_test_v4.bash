#!/bin/bash
filename="./input.list"
mkdir -p result.v4

full_file=result.v4/result.full_v4

rm -f ${full_file}

touch ${full_file}

echo "Starting..."
cat ${filename} | while read -r line; do
	for nthread in $(seq 64 64 1024); do
		echo ${line} ${nthread}
		./nsphere_cu_4 ${line} ${nthread} >>${full_file}
	done
done 

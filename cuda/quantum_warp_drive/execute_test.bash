#!/bin/bash
filename="./input.list"
mkdir -p result

base_file="result/result.base"
full_file=result/result.full
div_file=result/result.div

rm -f ${base_file} ${full_file} ${div_file}

touch ${base_file}
touch ${full_file}
touch ${div_file}

echo "Starting..."
cat ${filename} | while read -r line; do
	echo ${line}
	./nsphere_cu_3 ${line}>>${full_file}
	./nsphere_cu_2 ${line}>>${div_file}
	./nsphere ${line}>>${base_file}
done 

/**
 * Filename: nsphere.cu
 * Description: Nsphere implementation in CUDA
 * Author: Frederick Tan
 **/
#include <cuda.h>

#include <cmath>
#include <ctime>
#include <iostream>
#include <string>
#include <vector>

#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>

const long MAXDIM = 10;
const double RMIN = 2.0;
const double RMAX = 8.0;

using namespace std;

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, char *file, int line, bool abort=true)
{
    if (code != cudaSuccess)
    {
        fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
        if (abort) { exit(code); getchar(); }
    }
}

/**
 * Evaluate n**k where both are long integers
 **/
long powlong(long n, long k)
{
  long p = 1;
  for (long i = 0; i < k; ++i) p *= n;
  return p;
}

/**
 * Convert a decimal number into another base system - the individual
 * digits in the new base are stored in the index array.
 **/
void convert(long num, long base, long *index, long ndim)
{
  //const long ndim = index.size();
  for (long i = 0; i < ndim; ++i) index[i] = 0;
  long idx = 0;
  while (num != 0) {
    long rem = num % base;
    num = num / base;
    index[idx] = rem;
    ++idx;
  }
}

__global__ void count_in(long ndim, const long halfb, const long base, const double rsquare, const long ntotal, long *count)
{
  // Indices in x,y,z,....
  __shared__ long index[MAXDIM];
  __shared__ double rtestsq[MAXDIM];

  long n = blockIdx.x  + blockIdx.y * gridDim.x + gridDim.x * gridDim.y * blockIdx.z;
  long k = threadIdx.x;

  if (n < ntotal && k < ndim) {

    if (k == 0) {
      long idx = 0;
      int num = n;
      while (num != 0) {
        long rem = num % base;
        num = num / base;
        index[idx] = rem;
        ++idx;
      }
    }

    __syncthreads();

    double xk = index[k] - halfb;
    rtestsq[k] = xk * xk;

    __syncthreads();
    if (k == 0) {
      double totalrtestsq = 0.0;
      for (int i = 0; i < ndim; i++) {
        totalrtestsq += rtestsq[i];
      }
      //if (totalrtestsq < rsquare) count[n] = 1;
      if (totalrtestsq < rsquare) {
        atomicAdd((unsigned long long int*)count,
                  (unsigned long long int)1);
      }
    }
  }
}

int main(int argc, char* argv[])
{
  struct timeval tStart, tEnd;
  const long ntrials = 3;
  long *count = (long*)malloc(sizeof(long));
  size_t totalsize = sizeof(long);
  long *d_count;
  cudaMalloc(&d_count, totalsize);
  if (argc < 3) {
    cout << "Usage: " << argv[0] << " <radius> <n dimension>" << endl;
    exit(1);
  }

  const double radius = atof(argv[1]);
  const long ndim = atol(argv[2]);

  if ((radius < RMIN) || (radius > RMAX)) {
    cout << "Radius should be between " << RMIN << " and " << RMAX << "." << endl;
    exit(1);
  }

  if ((ndim < 1) || (ndim > MAXDIM)) {
    cout << "Dimension should be less than " << MAXDIM << "." << endl;
    exit(1);
  }

  *count = 0;
  gettimeofday(&tStart, NULL);
  gpuErrchk(cudaMemcpy(d_count, count, totalsize, cudaMemcpyHostToDevice));

  const long halfb = static_cast<long>(floor(radius));
  const long base = 2 * halfb + 1;
  const double rsquare = radius * radius;
  const long ntotal = powlong(base, ndim);

  // Let's figure out the number of blocks
  long remtotal = ntotal;
  long block1 = ntotal > 65535 ? 65535 : ntotal;
  remtotal = (remtotal / block1) + 1;
  long block2 = remtotal > 65535 ? 65535 : remtotal;
  remtotal = (remtotal / block2) + 1;
  long block3 = remtotal > 65535 ? 65535 : remtotal;

  dim3 bpg(block1, block2, block3);

  count_in<<<bpg, ndim>>>(ndim, halfb, base, rsquare, ntotal, d_count);

  gpuErrchk(cudaMemcpy(count, d_count, totalsize, cudaMemcpyDeviceToHost));
  gettimeofday(&tEnd, NULL);
  long int msEnd = tEnd.tv_sec * 1000 * 1000 + tEnd.tv_usec;
  long int msStart = tStart.tv_sec * 1000 * 1000 + tStart.tv_usec;
  std::cout << radius << "," << ndim << "," << ntotal << "," << (msEnd - msStart) << std::endl;

  free(count);
  cudaFree(d_count);
}
